package same_game;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class same {
	
	int filas;
	int columnas;
	int data[][];
	Stack<ArrayList<Pair>> pila;
	
	public same(int f, int c)
	{
		filas = f;
		columnas = c;
		data = new int[f][c];
	}
	
	
	public same(int [][] datos)
	{
		filas = datos.length;
		columnas = datos[0].length;
		data = new int[filas][columnas];
		for(int i = 0 ; i<filas ; i++)
		{
			for(int j = 0 ; j<columnas ; j++)
			{
				data[i][j] = datos[i][j];
			}
		}
	}
	
	
	public same(String dir)
	{
		try {
			ArrayList<ArrayList<Integer>> temp = read_matriz_int(dir);
			filas = temp.size();
			columnas = temp.get(0).size();
			data = new int[filas][columnas];
			for(int i = 0 ; i<filas ; i++)
			{
				for(int j = 0 ; j<columnas ; j++)
				{
					data[i][j] = temp.get(i).get(j);
				}
			}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	public int get_elem(int i , int j)
	{
		return	data[i][j];
	}
	
	
	public void set_elem(int i, int j, int val)
	{
		data[i][j] = val;
	}
	
	
	public void print()
	{
		for(int i = 0 ; i<filas ; i++)
		{
			for(int j = 0 ; j<columnas ; j++)
			{
				System.out.printf("%d\t",data[i][j]);
			}
			System.out.printf("\n\n");
		}
		System.out.printf("\n\n");
	}
	
	
	private ArrayList<ArrayList<Integer>> read_matriz_int(String dir) throws FileNotFoundException
	{
		ArrayList<ArrayList<Integer>> a = new ArrayList<ArrayList<Integer>>();
		Scanner input = new Scanner(new File(dir));
		while(input.hasNextLine())
		{
		    @SuppressWarnings("resource")
			Scanner colReader = new Scanner(input.nextLine());
		    ArrayList<Integer> col = new ArrayList<Integer>();
		    while(colReader.hasNextInt())
		    {
		        col.add(colReader.nextInt());
		    }
		    a.add(col);
		}
		input.close();
		return a;
	}
	
	
	public boolean encontrar_solucion()
	{
		if(is_data_vacio())
		{
			print();
			return true;
		}
		int i;	
		ArrayList<Pair> bloques= buscar_bloques();
		for(i = 0 ; i<bloques.size() ; i++)
		{
			same camino= new same(data);
			camino.buscar_adyacentes(bloques.get(i).getX(),bloques.get(i).getY());
			camino.hacer_caer();
			camino.mover_todo_izquierda();
			if(camino.encontrar_solucion())
			{
				System.out.printf("jugada: %d-%d\n",bloques.get(i).getX(),bloques.get(i).getY());
				print();
				return true;
			}
				
		}
		System.out.printf("no hay solucion\n");
		print();
		return false;
	}
	
	private void hacer_caer()
	{
		for(int c = 0 ; c<columnas ; c++)
		{
			int pos_vacia=-1;
			for( int i = 0 ; i < filas ; i++)
			{
				if(data[i][c] ==0)	pos_vacia =i;
			}
			
			for(int i = filas-1 ; i>=0 ; i--)
			{
				if(data[i][c]!=0 && i<pos_vacia)
				{
					data[pos_vacia][c] = data[i][c];
					data[i][c]= 0;
					pos_vacia--;
				}
			}
		}
		
	}
	
	private boolean is_columna_vacia(int n_columna)
	{
		for(int i = 0 ; i < filas ; i++)
		{
			if(data[i][n_columna]!=0)	return false;
		}
		return true;
	}
	
	private boolean is_data_vacio()
	{
		for(int i = 0 ; i < filas ;i++)
		{
			for(int j = 0 ; j<columnas ; j++)
			{
				if(data[i][j] !=0)
					return false;
			}
		}
		return true;
	}
	
	private void mover_izquierda(int destino, int n_columna)
	{
		for(int i = 0 ; i < filas ; i++)
		{
			data[i][destino] = data[i][n_columna];
			data[i][n_columna] = 0;
		}
	}
	
	private void mover_todo_izquierda()
	{		
		int pos_vacia = -1;
		for( int i = 0 ; i < columnas ; i++)
		{
			if(is_columna_vacia(i)){
				pos_vacia =i;
				break;
			}
		}
		//System.out.printf("pos vacia: %d\t",pos_vacia);
		
		for(int i = 0 ; i<columnas ; i++)
		{
			if( (!is_columna_vacia(i)) && i>pos_vacia)
			{
				//print();
				mover_izquierda(pos_vacia,i);
				//System.out.printf("moviendo de %d a %d\n", i,pos_vacia);
				//print();
				pos_vacia++;
			}
		}
	}
	
	private int buscar_adyacentes(int f, int c)
	{
		//System.out.printf("encontrado: %d-%d\n",f,c);
		int contador = 1;
		int temp =data[f][c];
		data[f][c]=0;
		if( c< columnas-1 && data[f][c+1] == temp)
			contador+= buscar_adyacentes(f,c+1);
		if( f<filas-1 && data[f+1][c] == temp)
			contador+=buscar_adyacentes(f+1,c);
		if( c>0 && data[f][c-1] == temp)
			contador+=buscar_adyacentes(f,c-1);
		if( f>0 && data[f-1][c] == temp)
			contador+=buscar_adyacentes(f-1,c);
		return contador;
	}
	
	private int[][] clonar_data(int[][] mat)
	{
		int[][] res = new int[filas][columnas];
		for(int i = 0 ; i<filas ; i++)
		{
			for(int j = 0 ; j<columnas ; j++)
			{
				res[i][j] = mat[i][j];
			}
		}
		return res;
	}
	

	private ArrayList<Pair> buscar_bloques()
	{
		ArrayList<Pair> list_temp = new ArrayList<Pair>();
		int[][] mat_temp = clonar_data(data);
		for(int i = 0 ; i<filas ; i++)
		{
			for(int j = 0 ; j<columnas ; j++)
			{
				if(data[i][j]!=0 && buscar_adyacentes(i,j)>1)
				{
					Pair clave = new Pair(i,j);
					list_temp.add(clave);
				}
			}
		}
		data = clonar_data(mat_temp);
		return list_temp;
	}


}
