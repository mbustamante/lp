# -*- coding:utf-8 -*-

'''
Created on 23/04/2014

@author: Miguel Bustamante
'''

from datetime import datetime
class Planificador:
    
    def __init__(self, p_duration, p_listFiles):
        self.listFiles = p_listFiles
        self.duration = datetime.strptime(p_duration,'%H:%M') - datetime.strptime('00','%M') 
        self.currentAvailability = {}
        self.keys=['mon','tue','thu','wed','fri','sun','sat']
        for key in self.keys:
            self.currentAvailability[key] = []
        
    
    def read_file(self,p_file):
        try:
            dirFile = open(p_file)
            data = dirFile.read()
            dirFile.close()
            return data
        except:
            print("no se encontro el directorio")
            
    
            
    def read_format_data(self,dirFile):
        format_data = {}
        week = [line.split(' ') for line in self.read_file("../resources/"+dirFile).split('\n')]
        for day in week:
            dayKey = day.pop(0)
            for i in range(len(day)):
                day[i] = [ datetime.strptime(start_end,'%H:%M') for start_end in day[i].split('-')]
            format_data[dayKey] = day
        return format_data
            
    def intersection(self,t1start, t1end,t2start, t2end):
        if (t1start <= t2start <= t2end <= t1end):
            return [t2start,t2end]
        elif (t1start <= t2start <= t1end):
            return [t2start,t1end]
        elif (t1start <= t2end <= t1end):
            return [t1start,t2end]
        elif (t2start <= t1start <= t1end <= t2end):
            return [t1start,t1end]
        else:
            return None
    
    def find_intersection(self,person1,person2):
        for key in self.keys:
            day = []
            day_person1 = person1[key]
            try:
                day_person2 = person2[key]
                for hour_day_person1 in day_person1:
                    for hour_day_person2 in day_person2:
                        hour = self.intersection(hour_day_person1[0], hour_day_person1[1], hour_day_person2[0], hour_day_person2[1])
                        if hour!= None and hour[1]-hour[0]>self.duration:
                            day.append(hour)
            except Exception as e:
                print(e)
            self.currentAvailability[key] = day
                
                    
         
        
    def find_dates(self):
        firstElem = self.listFiles.pop(0)
        firstElem = self.read_format_data(firstElem)
        for key in self.keys:
            try:
                self.currentAvailability[key] = firstElem[key]
            except Exception as e:
                print(e)
        
        for dirFile in self.listFiles:
            person =self.read_format_data(dirFile)
            self.find_intersection(self.currentAvailability,person)
        
        
        return self.currentAvailability
            
        
        
if __name__ == '__main__':
    x = Planificador('00:45',['a', 'b'])
    result = x.find_dates()
    for day in result.keys():
        for hour_available in result[day]:
            print(day, hour_available[0].time(), hour_available[1].time())
        