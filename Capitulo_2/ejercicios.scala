object Ejercicios{
    //ejercicio 1
	def clasificador(x:Int):Int = {
		if(x>1)	1
		else	if(0>x) -1	else	0
	}

    //ejercicio 2
    //var a = {}, a: Unit = (), esto indica que es del tipo Unit, y es analogo al void de C o Java.



    //ejercicio 3
    //  var x = {}
    //  var y = 0
    //  x = y = 1
    //  no bota error pero el valor de x no cambia


    //ejercicio 4
    //  for( i <- 0 to 10; j = 10-i )
    //      println(j)


    //ejercicio 5
    def countdown(n:Int){
        for( i <- 0 to n; j = n-i )
            println(j)
    }

    //ejercicio 6
    def unicodes(pal:String):Long={
        var acumulador:Long = 1
        for (c<- pal) acumulador *= c.toInt
        acumulador
    }


    //ejercicio 7
    def unicodes2(pal:String):Long={
        pal.foldLeft(1L)(_ * _.toInt)
    }


    //ejercicio 8
    def product(pal:String):Long={
        pal.foldLeft(1L)(_ * _.toInt)
    }


    //ejercicio 9
    def product_recursive(pal:String):Long={
        if(pal.isEmpty)
            1L
        else
            pal.head.toInt*product_recursive(pal.tail)
    }



    //ejercicio 10

    def pow(x:Int,n:Int):Float={
        if(0>n)
            1f/pow(x,-n)
        else
        {
            if(n== 0)
                1
            else
                if(n%2 ==0)
                {
                    var y = pow(x,n/2)
                    y*y
                }
                else
                {
                    var y = pow(x,n/2)
                    y*y*x
                }
        }
    }






	def main(args: Array[String]){

    	println(clasificador(10))
    	println(clasificador(-10))
    	println(clasificador(0))
        print("\n\n")

        countdown(5)
        print("\n\n")

        println(unicodes("Hello"))
        print("\n\n")

        println(unicodes2("Hello"))
        print("\n\n")

        println(product("Hello"))
        print("\n\n")

        println(product_recursive("Hello"))
        print("\n\n")


        println(pow(2,-6))

  	}
}