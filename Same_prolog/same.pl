longitud([],0).
longitud([_|Y],N):- longitud(Y,M),N is M+1.


pertenece(X,[X|_]).
pertenece(A,[_|Y]):-pertenece(A,Y).

juntar([],[],[]).
juntar([],Y,Y).
juntar(X,[],X).
juntar([A|B],Y,[A|C]):- juntar(B,Y,C).


eliminarIguales(_,[],[]).
eliminarIguales(L,[H|T],Z):-pertenece(H,L),eliminarIguales(L,T,Z).
eliminarIguales(L,[H|T],[H|A]):-eliminarIguales(L,T,A).


print_vector([]).
print_vector([A|Y]):-write(A),write('\t'),print_vector(Y).

print_matriz([]).
print_matriz([A|Y]):-print_vector(A),nl,nl,print_matriz(Y).


get_val([H|_],0,VAL):-VAL is H.
get_val([_|T],POS,VAL):-get_val(T,POS-1,AUX),VAL is AUX. 


%buscar_adyacentes([H|T],P):


same(M,[P1,P2]):-print_matriz(M),write(P1),write('-'),write(P2).

