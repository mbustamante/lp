% Cognitive systems engineering
% Example of a simple diagnosis program written in Prolog with example
% of ontological inference
%
% John Fox ............ version 1, November 2009
% 
% ----------------------------------------------------------------------
% 
% This prolog program shows how software can reason with ontological and
% other knowledge (lecture 1) in a medical diagnosis example. The
% program consists of a small knowledge base or ontology of medical
% facts (or "descriptions") which specify the symptoms caused by
% diseases or which are statistically associated with diseases, and a
% "class hierarchy" which describes terminological relationships and
% class/subclass relationships between diseaseses. In a logic program
% (lecture 2) the knowledge base can include rules of the form
% Conclusion :- Premises as well as facts. Here there are 2 sets of
% rules, one for deducing possible diagnoses given input symptoms
% (diagnosis/2), and another for reasoning over terminological and
% classificatory information (ontology/2).
% 
% ---------------------------------------------------------------------
% 
% To run the example, start SWI prolog and consult this source file 
% 
%      ?- consult(diagnosis1). (can be abbreviated [diagnosis1].)
% 
% then query the program with diagnosis(O,C) where O is a data item from
% the list [rash, skin_rash, fever, pyrexia, swollen_glands, red_spots]
% and C is a variable, indicated by a capital letter in the first
% position. For example
% 
%      ?- diagnosis(red_spots, Diagnosis).
%      
% The prolog inference engine will find all possible causes of red_spots
% for this knowledge base until it runs out of possibilities.

% -----------------------------------------------------------------------
% a knowledge base consisting of facts about a simple medical "domain"

causes(measles,red_spots).                % diseases cause symptoms
causes(meningitis,skin_rash).
causes(meningitis,fever).
causes(mumps,pyrexia).
causes(mumps,swollen_glands).
causes(flu,swollen_glands).
causes(flu,fever).

% non-causal associations

associated(avian_flu,foreign_travel).
associated(swine_flu,foreign_travel).

% another part of the knowledge base describes "a kind of"
% relationships, for terminology and class/subclass declarations

ako(swollen_glands,observation).
ako(rash,observation).
ako(skin_rash,rash).	      
ako(red_spots,skin_rash).
ako(fever,observation).
ako(pyrexia,fever).
ako(foreign_travel,observation).

ako(avian_flu,flu).
ako(swine_flu,flu).
ako(flu,disease).
ako(measles,disease).
ako(meningitis,disease).
ako(mumps,disease).
ako(disease,abnormality).

% a simple diagnosis procedure which determines which symptoms are
% observed and the conditions that could cause are associated with the
% symptom ("Heuristic classification").


% diagnosis/2 returns one or more possible abnormalities which could
% explain an observation

diagnosis(Observation,Condition) :-
	ontology(Observation,observation),
	ontology(Condition,abnormality),
	candidate(Observation,Condition).
diagnosis(Observation,Condition) :- fail.

% candidate/2 generates possible diagnoses from the facts in the
% knowledge base

candidate(Observation,Condition) :-
       (causes(Condition,Observation) ; associated(Condition,Observation)).
candidate(Observation,Condition) :-
	ontology(Observation,OClass), 
       (causes(Condition,OClass) ; associated(Condition,OClass)).
candidate(Observation,Condition) :-
	ontology(OClass,Observation),
       (causes(Condition,OClass) ; associated(Condition,OClass)).
candidate(Observation,Condition) :- fail.
	
% the ontology/2 rules infer whether concepts are in classes or not

ontology(Concept,Class) :-
	ako(Concept,Class).
ontology(Concept,Class) :-
	ako(Concept,SubClass),
	ontology(SubClass,Class).

%-----------------------------------------------------------------------
%
% PROBLEM write an alternative knowledge base for a different
% application, such as an equipment fault finding or car breakdown
% problem, which uses the same diagnosis logic but applied in a
% different domain.

causes(electrical_fault,car_wont_start).
causes(no_petrol,car_wont_start).

ako(wet_plugs, electrical_fault).
ako(car_wont_start,observation).
ako(electrical_fault,abnormality).

%-----------------------------------------------------------------------
%
% PROBLEM extend the candidate rules so that they can reason over causal
% relations or statistical associations of any degree (analogous to the
% ontology reasoner).

%-----------------------------------------------------------------------
%
% PROBLEM (hard for a new prolog programmer) extend the example to allow
% for combinations of observations to be input (e.g. fever and rash)
% 
%-----------------------------------------------------------------------
%
% PROBLEM (very hard for an inexperienced prolog programmer) further
% extend the example to take into account the strength of the
% association between observations and diseases and which generates an
% overall measure of confidence in each candidate diagnosis 