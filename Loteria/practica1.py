# -*- coding:utf-8 -*-


class loteria:

	def __init__(self):
		self.directorio = input("Ingrese directorio: ")
		self.loteria = input("Ingrese nombre de loteria: ")
		self.resultado = []
		self.m_apuestas = []

	def jugar(self):
		list_juegos = {"quina": None, "lotogol": None}
		list_juegos["quina"] = self.quina
		list_juegos["lotogol"] = self.lotogol
		list_juegos[self.loteria]()	

	def read_file(self,ruta_archivo):
		print(ruta_archivo)
		try:
			file = open(ruta_archivo)
			data = file.read()
			file.close()
			return data
		except:
			print("no se encontro el directorio")

	def llenar_apuestas_resultados(self):
		apuestas = self.read_file(self.directorio + "/" + self.loteria + "-apuesta")
		resultado = self.read_file(self.directorio + "/" + self.loteria + "-resultado")
		self.resultado = resultado.strip().split(" ")					
		apuestas = apuestas.strip().split('\n')
		self.m_apuestas = [ apuesta.split(' ') for apuesta in apuestas]


	def quina(self):
		list_precios = { "5":0.75,"6": 3, "7": 7.50}
		list_ganadores = [0,0,0,0,0,0]			#equivalente a {"0":0, "1":0, "2":0, "3":0, "4":0, "5":0}
		pozo = 0

		self.llenar_apuestas_resultados()
		
		'''a partir de aqui son puramente calculos'''
		for v_apuesta in self.m_apuestas:
			coincidencias = 0
			pozo+=list_precios[str(len(v_apuesta))]
			for apuesta in v_apuesta:
				if apuesta in self.resultado:
					coincidencias+=1
			list_ganadores[coincidencias]+=1
			pozo_premios = (pozo/1.045)*0.322
		quina= pozo_premios*0.35
		quadra= pozo_premios*0.25
		terno= pozo_premios*0.25
		#self.imprimir_data(total=pozo, pozo_de_premios=pozo_premios)
		print("total: %s\npozo de premios: %s\nQuina --> G: %s --P.I.: %s\nQuadra --> G: %s --P.I.: %s\nTerno --> G: %s --P.I.: %s\n"
			% (pozo, pozo_premios, list_ganadores[5], quina/list_ganadores[5], 
				list_ganadores[4],quadra/list_ganadores[4], list_ganadores[3],terno/list_ganadores[3]))

	def lotogol(self):
		list_precios = {"1":0.5,"2": 1, "4": 2}
		list_ganadores = [0,0,0,0,0,0]			#equivalente a {"0":0, "1":0, "2":0, "3":0, "4":0, "5":0}
		pozo = 0
		
		self.llenar_apuestas_resultados()
		
		'''esta parte es puramente calculos'''
		for v_apuesta in self.m_apuestas:
			n_apuestas = int(v_apuesta.pop(0))
			coincidencias = 0
			pozo+=list_precios[str(n_apuestas)]
			i = 0
			while i<len(v_apuesta):
				if v_apuesta[i]==self.resultado[i] and  v_apuesta[i+1]==self.resultado[i+1]:
					coincidencias+=1
				i+=2
			list_ganadores[coincidencias]+=(1*n_apuestas)

		pozo_premios = (pozo/1.044)*0.28
		quina= pozo_premios*0.4
		quadra= pozo_premios*0.3
		terno= pozo_premios*0.3
		print("total: %s\npozo de premios: %s\n5 Acier --> G: %s --P.I.: %s\n4 Acier --> G: %s --P.I.: %s\n3 Acier--> G: %s --P.I.: %s\n"
			% (pozo, pozo_premios, list_ganadores[5], quina/list_ganadores[5], 
				list_ganadores[4],quadra/list_ganadores[4], list_ganadores[3],terno/list_ganadores[3]))

		
if __name__ == '__main__':
	loteria_1 = loteria()
	loteria_1.jugar()
